FROM wordpress:cli
USER root
RUN apk update
RUN apk add --no-cache git openssh-client
USER www-data